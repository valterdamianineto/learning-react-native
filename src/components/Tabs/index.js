import React from 'react';

import Icon from 'react-native-vector-icons/MaterialIcons'
import { Container, TabsContainer, TabItem, TabText } from './styles';


export default function Tabs({ translateY }) {
    return (
        <Container style={{
            transform: [{
                translateY: translateY.interpolate({
                    inputRange: [0, 380],
                    outputRange: [0, 30],
                    extrapolate: 'clamp'
                })
            }],
            opacity: translateY.interpolate({
                inputRange: [0, 360],
                outputRange: [1, 0.3],
                extrapolate: 'clamp',
            })
        }}>
            <TabsContainer>
                <TabItem>
                    <Icon name="arrow-downward" size={24} color="#FFF" />
                    <TabText>Retirar pacote</TabText>
                </TabItem>
                <TabItem>
                    <Icon name="arrow-upward" size={24} color="#FFF" />
                    <TabText>Entregar pacote</TabText>
                </TabItem>
                <TabItem>
                    <Icon name="attach-money" size={24} color="#FFF" />
                    <TabText>Transações</TabText>
                </TabItem>
                <TabItem>
                    <Icon name="chat-bubble-outline" size={24} color="#FFF" />
                    <TabText>Suporte</TabText>
                </TabItem>
                <TabItem>
                    <Icon name="person-add" size={24} color="#FFF" />
                    <TabText>Indicar amigos</TabText>
                </TabItem>
            </TabsContainer>
        </Container>
    );
}