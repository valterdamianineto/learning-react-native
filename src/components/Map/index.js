import React, { Component } from 'react'
import MapView from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';

 

import { View } from 'react-native'

export default class Map extends Component {
    state = {
        region: null
    };

    async componentDidMount() {
        Geolocation.getCurrentPosition(
            ({ coords: { latitude, longitude } }) => {
                this.setState({
                    region: {
                        latitude,
                        longitude,
                        latitudeDelta: 0.005,
                        longitudeDelta: 0.008
                    }
                });
            },
            () => { },
            {
                timeout: 2000,
                enableHighAccuracy: true,
                maximumAge: 1000,
            }
        )
    }
    render() {
        const { region } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <MapView
                    style={{ flex: 1 }}
                    region={region}
                    showsUserLocation
                    loadingEnabled
                />
            </View>
        )
    }
}
