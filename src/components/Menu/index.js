import React from 'react';

import Icon from 'react-native-vector-icons/MaterialIcons';

import QRCode from 'react-native-qrcode';
import { Container, Code , Nav, NavItem, NavText, SignOutButton, SignOutButtonText } from './styles';

export default function Menu({ translateY }){
    return(
        <Container style={{
            opacity: translateY.interpolate({
                inputRange: [0, 250],
                outputRange: [0, 1]
            })  
        }}>
            <Code>
                <QRCode
                value="www.valterdamiani.com"
                size={80}
                fgColor="#1B4079"
                bgColor="#FFF"
                overflow="hidden"
                />
            </Code>
            <Nav>
                <NavItem>
                    <Icon name="credit-card" size={20} color= "#FFF"/>
                    <NavText>Meu saldo de entregas</NavText>
                </NavItem>
                <NavItem>
                    <Icon name="smartphone" size={20} color= "#FFF"/>
                    <NavText>Configurações do App</NavText>
                </NavItem>
                <NavItem>
                    <Icon name="person-outline" size={20} color= "#FFF"/>
                    <NavText>Perfil</NavText>
                </NavItem>
                <NavItem>
                    <Icon name="help-outline" size={20} color= "#FFF"/>
                    <NavText>Preciso de ajuda</NavText>
                </NavItem>
            </Nav>
            <SignOutButton onPress={() => {}}>
                <SignOutButtonText>SAIR DO APP</SignOutButtonText>
            </SignOutButton>
        </Container>

    )
}