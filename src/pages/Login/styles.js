import { Animated } from 'react-native'
import styled from 'styled-components/native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper'

export const Container = styled.View`
  flex: 1;
  /* background: #1B4079; */
  background: #006494;
  `

  export const Text = styled.Text`
    padding: 10px;
  `

export const Card = styled(Animated.View)`
  flex: 1;
  /* background: #FFF; */
  border-radius: 4px;
  margin: 50px 20px;
  /* height: 100%; */
  /* position: absolute;
  left: 0;
  right: 0;
  top: 0px; */
`

export const LoginEmail = styled.Text`
  margin: 20px;
  padding: 10px;
  border: 2px solid #FFF;
`

export const LoginPassword = styled.Text`
  margin: 20px;
  padding: 10px;
  border: 2px solid #FFF;
`