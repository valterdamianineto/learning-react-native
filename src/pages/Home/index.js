import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Map from '~/components/Map';


import { Animated } from 'react-native';
import { PanGestureHandler, State } from 'react-native-gesture-handler';

import Header from '~/components/Header';
import Menu from '~/components/Menu';
import Tabs from '~/components/Tabs';

import { Container, Content, Card, CardHeader, CardContent, CardFooter, Title, Description, Annotation } from './styles';


export default function Home() {
  let offset = 0;
  const translateY = new Animated.Value(0);

  const animatedEvent = Animated.event(
    [
      {
        nativeEvent: {
          translationY: translateY,
        }
      }
    ],
  { useNativeDriver: true }
  );

  function onHandlerStateChange(event){
    if(event.nativeEvent.oldState == State.ACTIVE){
      let opened = false;
      const { translationY } = event.nativeEvent;

      offset += translationY;

      if(translationY >= 100){
        opened = true;
      }else{
        translateY.setValue(offset);
        translateY.setOffset(0);
        offset = 0;
      }

      Animated.timing(translateY, {
        toValue: opened ? 360 : 0,
        duration: 300,
        useNativeDriver: true
      }).start(() => {
        offset = opened ? 360 : 0;
        translateY.setOffset(offset);
        translateY.setValue(0)
      })
    }
  }

  return (
      <Container>
        <Header/>
        <Content>
          <Menu translateY={ translateY }/>
          <PanGestureHandler
            onGestureEvent={animatedEvent}
            onHandlerStateChange={onHandlerStateChange}
          >
            <Card style={{
              transform: [{
                translateY: translateY.interpolate({
                  inputRange: [-350, 0, 360],
                  outputRange: [-30, 0, 360],
                  extrapolate: 'clamp',
                }),
              }]
            }}
            >
              <CardHeader>
                  <Title>Entrega DIsponivel</Title>
                  <Icon name="attach-money" size={28} color="#666" />
              </CardHeader>
              <CardContent>
                <Map/>
              </CardContent>
              <CardFooter>
                <Annotation>
                  <Icon name="location-on" size={28} color="#666" />
                  Farmácia Silva - Rua Andradas 2576, Capoeiras
                </Annotation>
              </CardFooter>
            </Card>
          </PanGestureHandler>
        </Content>
        <Tabs translateY={ translateY }/>
      </Container>
  )
}