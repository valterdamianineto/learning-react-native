import React from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import '~/config/ReactotronConfig';

import Home from '~/pages/Home';
import Login from '~/pages/Login';

const Stack = createStackNavigator();
const HeaderStyle = {
    backgroundColor: '#006494'
}

export default function App() {
    return (
        <NavigationContainer>
            <StatusBar barStyle="light-content" backgroundColor="#006494"/>
            <Stack.Navigator initialRouteName="Login">
                <Stack.Screen 
                    name="Home" 
                    component={Home} 
                    options={{
                        title: 'Acessar entregas',
                        headerStyle: HeaderStyle
                    }}
                />
                <Stack.Screen 
                    name="Login" 
                    component={Login}
                    options={{
                        title: 'Bem-vindo',
                        headerStyle: HeaderStyle,
                        headerTintColor: '#FFF'
                    }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}
 